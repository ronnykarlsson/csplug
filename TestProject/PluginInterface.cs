﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace TestProject
{
    /// <summary>
    /// PluginInterface abbreviated for easier use within the script-files.
    /// This is the interface-class which plugins can use to communicate with the program.
    /// </summary>
    public class PluginInterface
    {
        TextBox logTextBox;
        Random random;

        /// <summary>
        /// Constructor. Initialize variables.
        /// </summary>
        /// <param name="logTextBox"></param>
        public PluginInterface(TextBox logTextBox)
        {
            if (logTextBox == null) throw new ArgumentNullException("logTextBox");
            this.logTextBox = logTextBox;
            random = new Random();
        }

        public event EventHandler<RandomEventArgs> RandomEvent;

        /// <summary>
        /// Return a random number between 1 and 100.
        /// </summary>
        /// <returns></returns>
        public int GetRandomNumber()
        {
            int randomNumber = random.Next(100) + 1;

            if (RandomEvent != null) RandomEvent(this, new RandomEventArgs(randomNumber));

            return randomNumber;
        }

        /// <summary>
        /// Add <paramref name="message"/> to the log text.
        /// </summary>
        /// <param name="message">Message to send to the host program</param>
        public void SendMessage(string message)
        {
            logTextBox.Dispatcher.Invoke(new Action(() => logTextBox.Text = message + Environment.NewLine + logTextBox.Text));
        }
    }
}
