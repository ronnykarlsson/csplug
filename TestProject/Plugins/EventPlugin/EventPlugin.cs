﻿using CoolPlugins;
using CSPlug;
using TestProject;
using System;
using System.Timers;
using System.Reflection;

namespace CoolPlugins
{
    class EventPlugin : ICSPlug<PluginInterface>
    {
        private PluginInterface pi;
        private bool isLoaded = false;

        /// <summary>
        /// Initialization
        /// </summary>
        /// <param name="pluginInterface">Interface for communication with the host program.</param>
        public void Initialize(PluginInterface pluginInterface)
        {
            if (pluginInterface == null) throw new ArgumentNullException("pluginInterface");
            this.pi = pluginInterface;
        }

        /// <summary>
        /// Load plugin
        /// </summary>
        public void Load()
        {
            if (pi == null) throw new InvalidOperationException("pluginInterface required");
            if (isLoaded) return;

            pi.RandomEvent += RandomEventHandler;

            pi.SendMessage("EventPlugin loaded.");
            isLoaded = true;
        }

        /// <summary>
        /// Callback for the event when a random number is generated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void RandomEventHandler(object sender, RandomEventArgs e)
        {
            if (e.randomNumber > 70)
            {
                pi.SendMessage("EventPlugin: A number >70 was generated.");
            }
        }

        /// <summary>
        /// Unload plugin
        /// </summary>
        public void Unload()
        {
            if (!isLoaded) return;
            isLoaded = false;

            pi.RandomEvent -= RandomEventHandler;

            pi.SendMessage("EventPlugin unloaded.");
        }

        /// <summary>
        /// Determine whether plugin is loaded
        /// </summary>
        /// <returns>true if loaded, false otherwise</returns>
        public bool IsLoaded()
        {
            return isLoaded;
        }
    }
}