﻿using CoolPlugins;
using CSPlug;
using TestProject;
using System;
using System.Timers;
using System.Reflection;

namespace CoolPlugins
{
    class TimerPlugin : ICSPlug<PluginInterface>
    {
        private PluginInterface pi;
        private bool isLoaded = false;

        Timer timer;

        /// <summary>
        /// Initialization
        /// </summary>
        /// <param name="pluginInterface">Interface for communication with the host program.</param>
        public void Initialize(PluginInterface pluginInterface)
        {
            if (pluginInterface == null) throw new ArgumentNullException("pluginInterface");
            this.pi = pluginInterface;
        }

        /// <summary>
        /// Load plugin
        /// </summary>
        public void Load()
        {
            if (pi == null) throw new InvalidOperationException("pluginInterface required");

            // Load timer
            timer = new Timer(2000);
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            timer.Start();

            pi.SendMessage("TimerPlugin loaded.");
            isLoaded = true;
        }
        
        /// <summary>
        /// Tick-event from the timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            pi.SendMessage(String.Format("Hello from the timer plugin ({0})", pi.GetRandomNumber()));
        }

        /// <summary>
        /// Unload plugin
        /// </summary>
        public void Unload()
        {
            isLoaded = false;

            timer.Stop();
            timer = null;

            pi.SendMessage("TimerPlugin unloaded.");
        }

        /// <summary>
        /// Determine whether plugin is loaded
        /// </summary>
        /// <returns>true if loaded, false otherwise</returns>
        public bool IsLoaded()
        {
            return isLoaded;
        }
    }
}