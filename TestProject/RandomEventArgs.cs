﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestProject
{
    public class RandomEventArgs : EventArgs
    {
        public int randomNumber;

        public RandomEventArgs(int number)
        {
            randomNumber = number;
        }
    }
}
