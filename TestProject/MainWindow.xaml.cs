﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CSPlug;
using System.Diagnostics;

namespace TestProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// List of loaded plugins
        /// </summary>
        private List<ICSPlug<PluginInterface>> plugins;

        Plug plug;
        PluginInterface pluginInterface;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void hostWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Initialize variables
            plugins = new List<ICSPlug<PluginInterface>>();
            plug = Plug.Singleton;
            pluginInterface = new PluginInterface(logTextBox);
            
            // Temporary storage for loaded plugin
            ICSPlug<PluginInterface> plugin;

            plugin = plug.LoadPlugin(@"Plugins\TimerPlugin\", pluginInterface);
            if (plugin != null)
            {
                pluginInterface.SendMessage("TimerPlugin initialized.");
                plugin.Initialize(pluginInterface);
                plugins.Add(plugin);
            }

            plugin = plug.LoadPlugin(@"Plugins\EventPlugin\", pluginInterface);
            if (plugin != null)
            {
                pluginInterface.SendMessage("EventPlugin initialized.");
                plugin.Initialize(pluginInterface);
                plugins.Add(plugin);
            }
        }

        /// <summary>
        /// Load all plugins.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pluginsLoadButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (ICSPlug<PluginInterface> plugin in plugins)
            {
                if (!plugin.IsLoaded()) plugin.Load();
            }
        }

        /// <summary>
        /// Unload all plugins.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pluginsUnloadButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (ICSPlug<PluginInterface> plugin in plugins)
            {
                if (plugin.IsLoaded()) plugin.Unload();
            }
        }
    }
}
