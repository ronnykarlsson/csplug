﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.CodeDom.Compiler;

namespace CSPlug
{
    /// <summary>
    /// Main class for the DLL used to load plugins.
    /// </summary>
    public class Plug
    {
        /// <summary>
        /// Constructor
        /// </summary>
        private Plug()
        {
            compiler = new Compiler();
        }

        private Compiler compiler;

        /// <summary>
        /// Singleton instance of the class
        /// </summary>
        public static readonly Plug Singleton = new Plug();

        /// <summary>
        /// Load plugin with specified filename.
        /// </summary>
        /// <param name="fileName">Path for the plugin to be loaded.</param>
        /// <returns>An instance of the loaded plugin, null if load fails.</returns>
        /// <exception cref="CompilerException"/>
        public ICSPlug<T> LoadPlugin<T>(string directory, T pluginInterface)
        {
            string[] files;

            // Get files to compile
            directory = Path.GetFullPath(directory);
            files = Directory.GetFiles(directory);

            CompilerResults results = compiler.Compile(files);

            if (results.Errors.HasErrors)
            {
                // Compilation of cs-files wasn't successful
                StringBuilder sb = new StringBuilder();
                foreach (var outputString in results.Errors)
                {
                    sb.Append(outputString);
                }
                throw new CompilerException(sb.ToString());
            }

            // Compilation successful. Look for ICSPlug<T> to load
            Type[] classes = results.CompiledAssembly.GetTypes();

            // Iterate over classes in the assembly
            foreach (Type classType in classes)
            {
                // Iterate over interfaces implemented by the class
                Type typeInterface = classType.GetInterface("ICSPlug`1");
                if (typeInterface == typeof(ICSPlug<T>))
                {
                    ICSPlug<T> plugger = (ICSPlug<T>)Activator.CreateInstance(classType);
                    return plugger;
                }
            }

            return null;
        }
    }
}
