﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSPlug
{
    /// <summary>
    /// Interface implemented by plugins.
    /// </summary>
    public interface ICSPlug<T>
    {
        void Initialize(T commandInterface);
        void Load();
        void Unload();
        bool IsLoaded();
    }
}
