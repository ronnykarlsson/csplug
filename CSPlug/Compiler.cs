﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.CSharp;
using System.CodeDom.Compiler;

namespace CSPlug
{
    internal class Compiler
    {
        private CSharpCodeProvider provider;

        public CompilerParameters Parameters { get; private set; }

        public Compiler()
        {
            provider = new CSharpCodeProvider();
            Parameters = new CompilerParameters();

            // Only compile into memory, don't leave assemblies behind
            Parameters.GenerateInMemory = true;

            Parameters.ReferencedAssemblies.Add("System.dll");
            Parameters.ReferencedAssemblies.Add("CSPlug.dll");
            Parameters.ReferencedAssemblies.Add("TestProject.exe");
        }

        // Compile a plugin
        public CompilerResults Compile(string[] sourceFiles)
        {
            // Make sure parameters are OK
            if (sourceFiles == null) throw new ArgumentNullException("sourceFiles");
            CompilerResults results = null;

            // Compile
            results = provider.CompileAssemblyFromFile(Parameters, sourceFiles);

            // Give results of compilation back to calling method
            return results;
        }
    }
}
